﻿using UnityEngine;
using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

public class Interface : MonoBehaviour {

    public DBSCRIPT connectionParams;
    public string table = "Questions";
    

    /*private void Start()
    {
        Connect();
        InsertTEST();
    }*/

    SqlConnection connection;

    public bool Connect()
    {
        if (connection != null) return true;

        string connectionString = "";

        //connectionString = "Server= localhost; Database= Databhj,eldpwjnbase;Integrated Security = SSPI;";

        connectionString = "Server=DESKTOP-E1T1TGQ;Database=DB_Quiz;uid=root";

        /*connectionString += "Data Source=" + connectionParams.dataSource + ";";
        connectionString += "Initial Catalog=" + connectionParams.databaseName + ";";
        connectionString += "User ID=" + connectionParams.username + ";";
        connectionString += "Password=" + connectionParams.password + ";";*/

        connection = new SqlConnection(connectionString);
        //kek = new SqlConnection(connectionString);
        try
        {
            Debug.Log("Connecting...");
            connection.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
            return false;
        }

        Debug.Log("Connection established!");

        return true;
    }

    bool RunQuery(string query)
    {
        SqlCommand sql = new SqlCommand(query, connection);
        try
        {
            sql.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
            return false;
        }

        Debug.Log(query);

        return true;
    }

    bool RunQuery(string query, ref SqlDataReader reader)
    {
        SqlCommand sql = new SqlCommand(query, connection);
        try
        {
            reader = sql.ExecuteReader();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
            return false;
        }

        Debug.Log(query);

        return true;
    }

    bool InsertTEST()
    {
        string sqlInsert = "";
        sqlInsert += "INSERT INTO Category VALUES ('Film');";

        if (!RunQuery(sqlInsert)) return false;
        

        return true;
    }
}
