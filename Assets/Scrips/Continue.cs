﻿using System;
using System.Data.SqlClient;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Continue : MonoBehaviour {

    [SerializeField]
    private InputField inputField;

    private string userName;
    SqlConnection connection = new SqlConnection();

    public void OnClick()
    {
        //Need to pass UserName to DB in player table
        userName = inputField.text;
        Debug.Log(userName);
        //Connect();
        LauchScene();
    }

    private bool Connect()
    {
        connection.ConnectionString = "Server=DESKTOP-fgfdgthygnbfE1T1TGQ;Database=DB_Quiz;uid=root";
        try
        {
            Debug.Log("Connecting...");
            connection.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
            return false;
        }

        Debug.Log("Connected");
        InsertInto();
        return true;
    }

    private void InsertInto()
    {
        string Insert = "INSERT INTO Player VALUES('" + userName + "', 5)";

        SqlCommand sql = new SqlCommand(Insert, connection);
        //try
        {
            sql.ExecuteNonQuery();
        }
        /*catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + Insert);
            Debug.LogError("Error: " + e.Message);
        }*/

        //Debug.Log(Insert);
    }

    private void LauchScene()
    {
        SceneManager.LoadScene(1);
    }
}
