﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Data.SqlClient;
using System.Data;

public class ShowCat : MonoBehaviour {

    [SerializeField]
    private Dropdown dropdown;
    public List<string> list;

	void Start () {
        dropdown.ClearOptions();
        dropdown.AddOptions(list);
	}
}
